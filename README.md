## Description

Simple web crawling app using a distributed achitecture. It's purpose is to scrape a set of sitemap urls which have been retrieved from a database. The result of the scraping should be a list of product urls collected from the sitemaps with some metadata.

The system is meant to be run in at least two seperate containers, locally this can be accomplished by using the `docker-compose.yml` or any other container orchestration platform.

## How to run

1. `docker-compose build`
2. `docker-compose up -d --scale client=2`
3. `docker-compose logs -f -t`
4. `docker-compose down`


