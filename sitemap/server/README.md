# sitemap-server

### Description

The server is responsible for polling the database for any documents within the collection that are in need of an update (decided on by criteria in the server config). The server maintains a task list in memory of "tasks" needed to be completed and handles dispatching of tasks to client workers as well as the book-keeping for those tasks up to the point they are completed and an updated document payload can written back to the database.

### Domain Models

**Server Response:**

The response from the server

```
{
	id: String // Hashed UUID message ID
    message: String // The response verb from the server (JOBS, ACK_JOB)
    data: Task // The Dict payload containing the document to scrape - optional
}
```

**Client Request:**

The request sent to the server

```
{
 message: String // One of the request verbs (READY, JOB_DONE)
 data: Dict // A payload which contains the completed task
}
```

**Task (job):**

The Dict object which is stored in memory for the server to keep track of jobs.

```
{
	assigned_to: String // Hashed UUID client ID string
	sitemap: Dict //  The website document    
}
```

**Website Document**

```
{
    _id: ObjectId // MongoDB ObjectId
    url: String // The sitemap URL
    data: Array // A list of product URLs from the last scrape
    lastUpdated: Date // The last update time in MongoDB ISO format
}
```

### Entities

Database Connection:

This is a MongoDB found in `database/mongo.py` it opens a connection to the collection which stores the documents to be scraped

Producer (Task Dispatcher)

Found in `main.py`, the loop for the server and handles task dispatching and communication to clients via the `zmq` module.

### Messaging

Client Server Pattern:

The message passing component of the application is handled by the ZeroMQ library. It uses a client-server style pattern where a server constantly polls the database to refresh its list of pending tasks. As well as actively waiting for client connections which it will then dispatch tasks to. Inspired by the [zguide Ch 4 Reliable Request-Reply Patterns](http://zguide.zeromq.org/php:chapter4)

### Files Systems/External Volumes

None