#
# This service starts and only once retrieves sites from the database 
# It waits until clients connect and passes out a CHUNK of websites as
# jobs to clients. When all jobs are complete, it sleeps briefly and 
# does nothing to try to get more jobs...
#
# This could potentially run on a cron with at least one client service 
#
import json
import time
import os
import sys
import uuid 
import zmq
from database import mongo

CHUNK_SIZE = int(os.getenv('CHUNK_SIZE'))
SERVER_ID = uuid.uuid1().hex

cycles = 0
context = zmq.Context(1)
server = context.socket(zmq.REP)
HOST = os.getenv('HOST')
server.bind(HOST)

db = mongo.DB()

def main(): 
    print(f'[+] Started server {SERVER_ID}')
     
    job_list = get_sitemaps()
    print(f'[+] Found {len(job_list)} new jobs')
    
    while True:
        
        # All websites are scraped so we can shut off the server
        if len(job_list) < 1:
            print('[+] All jobs complete waiting for more...')
            time.sleep(5)
            
        # Wait for new requests from clients
        request = server.recv_json()
        msg = request['message']
        client_id = request['id']
        print(f'[+] Recieved {msg} from client {client_id}')
        
        # When a client informs us it's ready send a chunk of work to it
        if request['message'] == 'READY':
            jobs = get_jobs(client_id, job_list, CHUNK_SIZE)
            send_message('JOBS', jobs)
                    
        # A job was completed so mark that job as done in the list
        elif request['message'] == 'JOB_DONE':
            
            result = db.update_products(request['data']['products'])
            print(f'[+] Database results: {result}')
            
            for job in range(len(job_list)):
                if job_list[job]['sitemap'] == request['data']['sitemap']:
                    del job_list[job]
                    break
            send_message('ACK_JOB')

    server.close()
    context.term()

def get_jobs(client_id, job_list, limit=CHUNK_SIZE):
    """
        Returns a list of active jobs from the job list
        job_list - passed by reference 
    """
    jobs = []
    for job in job_list:
        if job['assigned_to'] == client_id:
            jobs.append(job)

    if len(jobs) > 0:
        print(f'[!] Found {len(jobs)} outstanding jobs for client {client_id}')
        
    else:
        for i in range(len(job_list)):
            # if we reached the max number of jobs stop
            if len(jobs) == limit: break
            
            # append all the jobs to the message payload for the response
            if job_list[i]['assigned_to'] is False:
                jobs.append(job_list[i])
                job_list[i]['assigned_to'] = client_id
                print(f'[+] Assigning {len(jobs)} jobs to client {client_id}')
    return jobs

def send_message(message, data=None, id=SERVER_ID):
    """
        Send a response as JSON to the client
    """
    response = {
        'id': id,
        'message': message,
        'data': data
    }
    server.send_json(response)
    print(f'[+] Sent: {response} to client')
        
def get_sitemaps():
    """
        Return a list of sitemaps from the database
    """
    sitemaps = db.get_sites()
    job_list = []
    for m in sitemaps:
        job = {
            'sitemap': m,
            'assigned_to': False
        }
        job_list.append(job)
    return job_list

if __name__ == '__main__':
    main()