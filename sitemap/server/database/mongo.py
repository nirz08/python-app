import pymongo
from pymongo import UpdateOne
import dns
import bson
import os
import json
from dateutil import parser
from urllib.parse import urlparse


class DB:
    # TODO - add connection string data to constructor
    def __init__(self, conn='', database=''):
        self.conn = conn
        self.database = database
        self.client = pymongo.MongoClient(os.getenv("DB_CONN"))

    def get_sites(self):
        """
            Return all website sitemaps as a list
        """
        try:
            cursor = self.client.budbot.sites.aggregate([
                { '$unwind': '$sitemaps' },
                { '$group': {'_id':'null', 'sitemaps': {'$push': '$sitemaps'}} },
                { '$project': {'_id':0, 'sitemaps': '$sitemaps'} }
            ])
            result = list(cursor)[0]['sitemaps']
        except Exception as e:
            print(e)
        else:
            return result

    def get_products_by_lastUpdate(self, domain):
        """
            Return all products for a specific domain that have been updated at least once
        """
        try:
            cursor = self.client.budbot.products.find( {'domain': domain }, { "url":1, 'lastUpdate':1 })
        except Exception as e:
            print(e)
        else:
            return list(cursor)

    def update_products(self, new_products):
        """
            Update a list of products given the new_products payload
        """
        try:
            operations = []
            for product in new_products:
                product['lastModified'] = parser.parse(product['lastModified'])
                
                operations.append(
                    UpdateOne( { 'url': product['url'] }, { '$set': { 'lastModified': product['lastModified'] } }, upsert=True)
                )
            result = self.client.budbot.products_v2.bulk_write(operations)
            result = result.bulk_api_result
            result = {
                'nInserted': result['nInserted'],
                'nUpserted': result['nUpserted']
            }
        except Exception as e:
            print(e)
        else:
            return result