import operator
import requests
import datetime
import time
import os


class Scraper:

    def __init__(self):
         # todo extract these out to global scraper config
        self.MAX_ATTEMPTS = 5
        self.TIMEOUT = 20
        self.DELAY = 5
        self.HEADERS = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    def download_page(self, url, attempts=0):
        """
            Handles collection of data from the webpage
            Returns a dict of metadata about the given url or
            a dict containing an error object
        """
        attempts = attempts + 1

        # Attempt to download the webpage
        try:
            if attempts > self.MAX_ATTEMPTS:
                raise Exception('Max connection attempts exceeded')

            response = self.downloader(url)
            response.raise_for_status()  # handle http errors as exception

            # If the page downloaded successfully return the page content
            return response

        # Retry connection a predetermined amount of times
        except requests.exceptions.Timeout as e:
            print("[-] Timeout occurred trying to download again, attempt: " + attempts)
            response = requests.get(
                url, headers=self.HEADERS, timeout=self.TIMEOUT)
            response.raise_for_status()  # handle http errors as exception

        # Url is bad and we should try a different one
        except requests.exceptions.TooManyRedirects as e:
            print("[-] Too many redirects occurred, likely bad URL")
            raise e

        # Handle 401 or 404 errors gracefully - mark the url as dead
        except requests.exceptions.HTTPError as e:
            print("[-] HTTP error occurred, cannot download webpage")
            raise e

        # Catastrophic error - todo this might catch my custom exception
        except requests.exceptions.RequestException as e:
            print("[-] Runtime error, exiting")
            raise e

        # Catch the only other exception - max timeout attempts reached
        except Exception as e:
            print(e)
            raise e

    def scrape_sitemap(self, url):
        """
            Attempt parse the given URL for metadata. Regardless of scraping success
            the results are returned so they can be stored in the DB for future reference.
        """

        # Download page, run tests and return any results
        try:
            page_response = self.download_page(url)
            page_content = BeautifulSoup(
                page_response.content, features="lxml")

            product_urls = page_content.find_all("url")
            products = []
            counter = 1
            for url in product_urls:
                product = self.analyze_sitemap_url(url)
                if product:
                    products.append(product)
                    counter += 1
            return products

        except Exception as e:
            raise e
            # return [{
            #     'url': url,
            #     'error': e,
            #     'lastUpdate': datetime.datetime.now()
            # }]

    def analyze_sitemap_url(self, url):
        """
            Attempt to pattern match a product URL based on the given string.
            Returns a dict entry with the url and the lastModified date
        """
        patterns = ['/product/', '/products/']
        for p in patterns:
            if p in url.loc.string:
                return {
                    'url': url.loc.string,
                    'lastModified': url.lastmod.string
                }
        return False

    def downloader(self, url):
        """
            Wraps the response library with a custom delay backoff between requests
            to prevent overloading the site. Only necessary when downloading multiple
            pages from the same domain.
        """
        t0 = time.time()
        response = requests.get(
            url, headers=self.HEADERS, timeout=self.TIMEOUT)
        response.raise_for_status()  # handle http errors as exception
        response_delay = time.time() - t0
        # wait 10x longer than it took them to respond
        time.sleep(self.DELAY * response_delay)
        return response     