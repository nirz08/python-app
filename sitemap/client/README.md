# sitemap-client

### Description

The client is responsible for sending requests to the server for any documents within the collection that are in need of an update (decided on by criteria in the server config). The client iterates through the list of tasks recieved and attempts to crawl the sitemap and retrieve the results from the page. Once completed the client will send the payload in a requst back to the server.

### Domain Models

**Client Request:**

The request sent to the server

```
{
 message: String // One of the request verbs (READY, JOB_DONE)
 data: Dict // A payload which contains the completed task
}
```

**Server Response:**

The response from the server

```
{
	id: String // Hashed UUID message ID
    message: String // The response verb from the server (JOBS, ACK_JOB)
    data: Task // The Dict payload containing the document to scrape - optional
}
```

**Task (job):**

Found within the server response it contains the task to be done

```
{
	assigned_to: String // Hashed UUID client ID string
	sitemap: Dict //  The website document    
}
```

**Website Document:**

```
{
    _id: ObjectId // MongoDB ObjectId
    url: String // The sitemap URL
    data: Array // A list of product URLs from the last scrape
    lastUpdated: Date // The last update time in MongoDB ISO format
}
```

### Entities

**Scraper:**

The `Scraper` class found in `scraper.py`  makes HTTP requests to website's sitemap target using the `requests` module. String pattern matching is done on the sitemap content to filter out product urls. The results can then be sent back to the server.

**Consumer (Task Reciever):**

Found in `main.py`, the loop for the client and handles task recieving and communication to the server via the `zmq` module.

### Messaging

Client Server Pattern:

The message passing component of the application is handled by the ZeroMQ library. It uses a client-server style pattern where a server constantly polls the database to refresh its list of pending tasks. As well as actively waiting for client connections which it will then dispatch tasks to. Inspired by the [zguide Ch 4 Reliable Request-Reply Patterns](http://zguide.zeromq.org/php:chapter4)

### Files Systems/External Volumes

None