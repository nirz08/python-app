
import json
import os
import time
import uuid
import zmq
from scraper import Scraper

REQUEST_TIMEOUT = int(os.getenv('REQUEST_TIMEOUT'))
REQUEST_RETRIES = int(os.getenv('REQUEST_RETRIES'))
SERVER_ENDPOINT = os.getenv('SERVER_ENDPOINT')
CLIENT_ID = uuid.uuid1().hex

# ZMQ setup
context = zmq.Context(1)
client = context.socket(zmq.REQ)
client.connect(SERVER_ENDPOINT)
poll = zmq.Poller()
poll.register(client, zmq.POLLIN)


def main():
    global client
    job_list = []
    scraper = Scraper()
    retries_left = REQUEST_RETRIES
    request = {
        'message': None,
        'data': None
    }

    # A limited attempts to contact the server will be made before quitting
    while retries_left:

        # First ask for more jobs if the list is empty
        if len(job_list) < 1:
            request['message'] = 'READY'
            request['data'] = None
            send_message('READY')

        # Scrape the next sitemap in the job list
        else:
            sitemap = job_list[0]
            print(f'[+] Scraping sitemap: {sitemap}')
            products = scraper.scrape_sitemap(sitemap)
            # Send job complete back to server with the products found
            request['message'] = 'JOB_DONE'
            request['data'] = {'sitemap': sitemap, 'products': products}
            send_message('JOB_DONE', {'sitemap': sitemap, 'products': products })

        # Response handling loop
        expect_reply = True
        while expect_reply:
            socks = dict(poll.poll(REQUEST_TIMEOUT))
            if socks.get(client) == zmq.POLLIN:

                # We recieved a response from the server
                reply = client.recv_json()

                # Empty response so break and retry sending request
                if not reply:
                    break

                # The reply contained a list of jobs so start working
                if reply['message'] == 'JOBS':

                    # There is no new jobs so we can power down for now
                    if len(reply['data']) < 1:
                        print('[+] There is no work for me sleeping...')
                        retries_left = REQUEST_RETRIES
                        expect_reply = False
                        time.sleep(5)
                    else:
                        # Add new jobs to the list
                        print(f'[+] Recieved new jobs {reply}')
                        job_list = list(map(lambda x: x['sitemap'], reply['data']))
                        retries_left = REQUEST_RETRIES
                        expect_reply = False

                # Client can continue scraping if the server confirms the last sitemap was removed
                elif reply['message'] == 'ACK_JOB':
                    print(f'[+] Confirmed {job_list[0]} was removed')
                    del job_list[0]
                    retries_left = REQUEST_RETRIES
                    expect_reply = False

                # Something unhandled was sent by the server...
                else:
                    print("[-] Malformed reply from server: %s" % reply)

            # This is all error handling and retrying...
            else:
                print("[!] No response from server, retrying…")
                # Socket is confused. Close and remove it.
                client.setsockopt(zmq.LINGER, 0)
                client.close()
                poll.unregister(client)
                retries_left -= 1
                if retries_left == 0:
                    print("[-] Server seems to be offline, abandoning")
                    break
                print("[!] Reconnecting and resending (%s)" % request)
                # Create new connection
                client = context.socket(zmq.REQ)
                client.connect(SERVER_ENDPOINT)
                poll.register(client, zmq.POLLIN)
                send_message(request['message'], request['data'])

    # This only happens after every shuts off
    context.term()


def send_message(message, data=None, id=CLIENT_ID):
    """
        Send a response as JSON to the client
    """
    response = {
        'id': id,
        'message': message,
        'data': data
    }
    client.send_json(response)
    print(f'[+] Sent: {message} to server')


if __name__ == '__main__':
    main()
